# -*- coding: utf-8 -*-
from django.shortcuts import get_object_or_404, render, get_list_or_404, render_to_response
from django.http import HttpResponseRedirect, HttpResponse, HttpRequest
from django.core.urlresolvers import reverse
from django.template import Context, loader, RequestContext
from django.http import Http404	
from administration.models import Post, User, DBPurchase, Paypal
from administration.forms import *
from django.utils import timezone
from django.contrib.auth import authenticate, login, logout
from django.db.models import Q, Count, Min, Sum, Avg
from django.db import connection, transaction
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import user_passes_test
from django.core.mail import send_mail, EmailMultiAlternatives
import hashlib
import urllib, urllib2
import time, datetime
from helpers.ajax import ajax_view, AjaxError
from django.core.exceptions import ObjectDoesNotExist
from django_datatables_view.base_datatable_view import BaseDatatableView



def about(request):	
	context = {'myPlace':'about'} # addhering to the dictionery  (general)context new data, the place that i am
	return render(request,'about.html',context)	

def bots(request):	
	context = {'category':'Bots','title':'Bots','myPlace':'bots'}
	context['menuState']='Development'
	return render(request, 'bots.html', context )

def blog(request,blog_id):
	context = dict()
	posts = get_list_or_404(Post.objects.order_by('-pub_date'))[0:3]
	postChoose=Post.objects.get(id=blog_id)	
	context['dataBlog4']=posts # addhering to the dictionery  (general)context new data
	context['dataBlogChoose']=postChoose # addhering to the dictionery (general)context new data
	return render(request,'blog.html',context)

def change_password (request):
	if request.method == 'POST':
		form = ChangePasswordForm(user = request.user, data = request.POST)
		if form.is_valid():
			form.save()
			return HttpResponseRedirect( reverse ('administration:profile') )
	else:
		form = ChangePasswordForm(user = request.user)	
	return render (request, 'changepassword.html', {'form':form})

def conditions(request):
	context={'title':'Condiciones' }	
	return render(request,'conditions.html',context)

#@csrf_exempt
def confirm_purchase (request):
	if request.POST:				
		print "In Confirm POST", len(request.POST)
		# for a in request.POST:
		# 	print '\tPost', a,"=",request.POST[a]		
		parameters=request.POST.copy()
		if parameters:	
			print "entrass<-----------------------------------------------"
			if parameters['payment_status']=="Completed": # and parameters['txn_id']==var_txn 
				print "parameters['custom'] ",parameters['custom'] #to see my id_superuser
				parameters['cmd']='_notify-validate'
				params = urllib.urlencode(parameters)
				if parameters['custom']=='73' or parameters['custom']=='1' :
					urlPostBack='https://www.sandbox.paypal.com/cgi-bin/webscr'
				else:
					urlPostBack='https://www.paypal.es/cgi-bin/webscr'

				fo=urllib2.urlopen(urlPostBack, params)
				status=fo.read()
				if status == "VERIFIED":
					print "right!!"
					print status			
					userId=parameters['custom']					
					print "request.user.pk: ",userId
					paypal=Paypal(user_id=userId,payment_status=parameters['payment_status'],
								  receiver_email=parameters['receiver_email'],payment_sail=parameters['payment_gross'],
								  txn_id=parameters['txn_id'],residence_country=parameters['residence_country'],
								  item_name=parameters['item_name'],payer_id=parameters['payer_id'],
								  ipn_track_id=parameters['ipn_track_id'])	
					paypal.save()
					# userToSail=User.objects.get(pk=userId)
					userSail=DBPurchase(user_id=userId,application_id=1,account_type=1,start_using_date=time.time(), time_license=2592000)								  				
					userSail.save()
					print "after<--------------------------"
				else:
					print "fraud!!"
					return HttpResponse('You are a fraud!')

			# here should be if payment status is not complete, redirect to other page

		else:
			print "don't exist parameters!!<---------------------------------"
			return render (request, 'noAuthorized.html')
		
	else:
		if request.user.is_authenticated():				
			print "SIIII esta auth since down<-------------------------"
			idUserCurrent=request.user.pk
			print "idUserCurrent: ",idUserCurrent
			try:
				toAnalize=Paypal.objects.get(user_id=idUserCurrent,active_confirm_purcha=0)
			except ObjectDoesNotExist:
				return render (request, 'noAuthorized.html')
			# toAnalize=Paypal.objects.get(user_id=idUserCurrent,active_confirm_purcha=0)	
			
			print "toAnalize.active_confirm_purcha<------------------: ",toAnalize

			if toAnalize.active_confirm_purcha == 0:					
				print "entrassaa  form<----------------------------------"
				toAnalize.active_confirm_purcha=1		
				toAnalize.save()
				return render (request, 'confirmpurchase.html')
			else:	
				return render (request, 'noAuthorized.html')	

		else:
			print "NOOOO esta auth since down<-------------------------"
			return render (request, 'noAuthorized.html')	

		# print "verified and redirec by paypal POST!!<----------------------------------------"
		# # we need verificate if is active first view of conformpurchase !!

		# return render (request, 'confirmpurchase.html')
	

def contact(request):
	context = {'myPlace':'contact'} # addhering to the dictionery  (general)context new data, the place that i am
	return render(request,'contact.html',context)

def development(request):	 
	context={'menuState':'Development'} # addhering to the dictionery  (general)context new data, the place that i am
	context['title']="Desarrollo"#'Development'
	return render(request,'development.html',context)					

def index(request):
	context={'menuState':'Home','myPlace':'index'}
	try:
		post = Post.objects.order_by('-pub_date')[0] # to know  the last post publicated to "every page" for that is here!
	except:
		post = None	
	context['dataBlog']= post	#saving to sent at template to "every page" for that is here!
	context['title']= "Inicio"#"Home"	#saving to sent at template to "every page" for that is here!
	print "Hello world in console"
	#post=Post.objects.order_by('-pub_date')[0]	
	return render(request,'index.html',context)

def login_user (request):
	path="administration:specificBot"
	print path
	if request.method == 'POST':
		form = LoginForm(request.POST)
		if form.is_valid():
			email = form.cleaned_data['email']
			password = form.cleaned_data['password']
			user = authenticate(username=email, password=password)
			login(request, user)
			return HttpResponseRedirect( reverse (path,args=("perfectAimBot",)) )
	else:		
		form = LoginForm()
	return render (request, 'login.html', {'form':form } )


def logout_user(request):
	logout(request)	
	return HttpResponseRedirect( reverse ('administration:index') )

def profile (request):
	user_id = request.user.pk
	cursor = connection.cursor()
	cursor.execute("SELECT account_type, start_using_date, time_license, is_expired, COUNT(A.db_purchase_id) as 'Num Accounts', D.name FROM administration_dbaccountpurchase A RIGHT JOIN administration_dbpurchase P ON P.id = A.db_purchase_id JOIN administration_application D ON P.application_id = D.id  WHERE P.user_id = %i GROUP BY P.id;" % user_id)
	context = {'purchases':cursor.fetchall()}
	if request.user.is_superuser:
		discount = PaymentRegister.objects.aggregate(Sum('bank_discount'))['bank_discount__sum']
		amount = PaymentRegister.objects.aggregate(Sum('amount'))['amount__sum']
		context['total_amount'] = amount - discount
	return render (request, 'profile.html', context)

def signup (request):
	if request.user.is_authenticated():
		return render (request, 'index.html')
	if request.method == 'POST':
		form = RegisterForm(request.POST)
		if form.is_valid():
			password = form.save()
			print "new password", password
			return HttpResponseRedirect ( reverse ('administration:login') ) #depends where will be the redirec after 
	else:
		form = RegisterForm()
	return render (request, 'signup.html', {'form':form} )

@user_passes_test(lambda u: u.is_superuser)
def sendEmailAimbot (request):
	# return HttpResponseRedirect ( reverse ('administration:profile') )
	if request.method == 'POST':
		form = SendEmailAimbot(request.POST)
		if form.is_valid():
			return HttpResponseRedirect ( reverse ('administration:profile') )
	else: 
		form = SendEmailAimbot()
	return render (request, 'sendmail.html', {'form':form} )

@user_passes_test(lambda u: u.is_superuser)
def showUserProfile(request, pk):	
	suser = User.objects.get (pk = pk)
	cursor = connection.cursor()
	cursor.execute("SELECT account_type, start_using_date, time_license, is_expired, COUNT(A.db_purchase_id) as 'Num Accounts', D.name FROM administration_dbaccountpurchase A RIGHT JOIN administration_dbpurchase P ON P.id = A.db_purchase_id JOIN administration_application D ON P.application_id = D.id  WHERE P.user_id = %i GROUP BY P.id;" % suser.pk)
	return render (request, 'userprofile.html', {'purchases':cursor.fetchall(), 'suser':suser} )
	
@user_passes_test(lambda u: u.is_superuser)
def activatePremium (request):
	if request.method == 'POST':
		print  "Here"
		form = ActivatePremium(who_activated = request.user.pk, data = request.POST)
		if form.is_valid():
			form.save()
			return HttpResponseRedirect ( reverse ('administration:profile') )
	else: 
		form = ActivatePremium(who_activated = request.user.pk)	
	return render (request, 'activatepremium.html', {'form':form } )

@ajax_view
def sendVoucher (request):	
	try:		
		numvoucher = request.GET.get('numvoucher', '')
		user_email = request.GET.get('username', '')
		subject, from_email = u'Voucher: Nueva Compra', 'NinjaxSolutions-DragonBot', 
		to_list = ["ninjaxcompany@gmail.com", "jean.luque@ucsp.edu.pe"]
		text_content = "Numero Voucher: %s \nUsuario: %s" % (numvoucher, request.user.username)
		send_mail(subject, text_content, from_email, to_list)
	except:
		print "Error sending voucher"

def specificBot(request, bot):
	context={'category':'Bots','title':'Bots','myPlace':'specificBot', 'specificBot':bot}
	# va='back:'+request.path
	# q = QueryDict(va) 
	try:
		if_is_premium = DBPurchase.objects.get(user_id=request.user.pk)
		print "if_is_premium: ",if_is_premium.account_type
	except:
		if_is_premium=None
	premium=1 #reverse to down
	free=2 #reverse to up

	if if_is_premium:
		if if_is_premium.account_type==premium:
			context["iconDownload"]="premium"

		if if_is_premium.account_type==free:
			context["iconDownload"]="freeDownloaded"		

	else:
		context["iconDownload"]="free"
	if bot=="quickRoomBot":
		context['subMenuState']='Quick Room Bot'
	else:#to changue when exist more bots
		context['subMenuState']='Perfect Aim Bot'

	context['numUser']=request.user.pk
	return render(request, 'specificBot.html', context )

# @transaction.commit_on_success()
# @transaction.commit_manually
def events(request):	
	# do_stuff()
	context={'category':'Events','title':'Event','myPlace':'events' }	
	accounts_to_give=99 #numof acountss
	users_free= DBPurchase.objects.filter(account_type=1)
	if request.method == 'POST':		
		form = EventForm(session_key = request.session.session_key, data = request.POST)
		if form.is_valid():
			dataForm = form.save()
			print "ended",dataForm["ended"]
			print "password: ",dataForm["password"]
			if dataForm["ended"]==0:
				return render (request, 'thanks.html')
				# return HttpResponseRedirect( reverse ('administration:login') ) #depends where will be the redirec after 
			context["ended"]=1	
			context["notice"]="Las cuentas Premium se han terminado, PERO tu cuenta Free se ha creado; podras acceder a descargar el bot despues del evento weekend y tambien COMPRARLO"
			num_users=users_free.count()
			print "num_users:",num_users
			num_accounts_yet=accounts_to_give-num_users #because is one acoount left when the register is maden
			context["num_accounts_yet"]=num_accounts_yet
		
	else:
		num_users=users_free.count()
		print "num_users", num_users
		num_accounts_yet=accounts_to_give-num_users #because is one acoount left when the register is maden
		context["num_accounts_yet"]=num_accounts_yet
		form = LoginForm()
			
	context["formEvent"]=form 
	return render(request,'events.html',context)

class PremiumListJSON(BaseDatatableView):
	model = DBPurchase
	columns = ['id', 'user', 'account_type', 'start_using_date', 'time_license']
	order_columns = ['-start_using_date', '-time_license', '-user', '-account_type']
	max_display_length = 100

	# def get_initial_queryset (self):
	# 	return DBPurchase.objects.filter(account_type = 1):D

	def render_column(self, row, column):		
		if column == 'user':
			return '<a href="%s"> %s </a>' %( reverse('administration:showuserprofile', args = [row.user.pk]), row.user.username )
		elif column == 'start_using_date':
			return time.strftime('%d/%m/%Y', time.localtime(row.start_using_date))
		elif column == 'time_license':
			real_time = int(row.time_license)/86400
			if real_time >= 360:
				real_time = real_time/360
				if real_time == 1: return "%i año" % real_time
				else: return "%i años" % real_time
			elif real_time >= 30:
				real_time = real_time/30
				if real_time == 1: return "%i mes" % real_time
				else: return "%i meses" % real_time
			else:
				if real_time == 1: return "%i día" % real_time
				else: return "%i días" % real_time
		else:
			return super(PremiumListJSON, self).render_column(row, column)

	def filter_queryset(self, qs):
		sSearch = self.request.GET.get('sSearch', None)
		premium_search = sSearch.find('p:')
		free_search = sSearch.find('f:')
		if premium_search == 0:
			sSearch = sSearch.replace ('p:', '').strip()
			qs = qs.filter(account_type = 1)
		elif free_search == 0:
			sSearch = sSearch.replace ('f:', '').strip()
			qs = qs.filter(account_type = 0)
		if sSearch:
			# qs = qs.filter(Q(razon_social__istartswith=sSearch) | Q(ruc__istartswith=sSearch))
			qs = qs.filter(user__username__istartswith=sSearch)
		return qs

class PaymentListJSON (BaseDatatableView):
	model = PaymentRegister
	columns = ['date_payment', 'who_activate', 'purchase', 'amount', 'bank_discount', 'location_voucher']
	order_columns = ['-date_payment']
	max_display_length = 100

	def render_column(self, row, column):	
		if column == 'who_activate':
			return row.who_activate.username
		elif column == 'purchase':
			return row.purchase.user.username
		else:
			return super(PaymentListJSON, self).render_column(row, column)

	def filter_queryset(self, qs):
		sSearch = self.request.GET.get('sSearch', None)
		who_activate_search = sSearch.find('w:')
		user_search = sSearch.find('u:')
		if who_activate_search == 0:
			sSearch = sSearch.replace ('w:', '').strip()
			qs = qs.filter(who_activate__username__istartswith=sSearch)
		elif user_search == 0:
			sSearch = sSearch.replace ('u:', '').strip()
			qs = qs.filter(purchase__user__username__istartswith=sSearch)
		return qs