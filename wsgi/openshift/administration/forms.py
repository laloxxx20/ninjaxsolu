# -*- coding: utf-8 -*-
from django import forms
from administration.models import User
from django.core.exceptions import ValidationError
from captcha.fields import ReCaptchaField
from django.core.validators import validate_email
from administration.control import *
import hashlib
import time, datetime
import string
from random import sample, choice
from django.core.mail import send_mail, EmailMultiAlternatives
from django.contrib.auth import authenticate
from django.http import HttpResponse
from django.db import connection
from models import *
from django.utils.translation import ugettext_lazy

class ChangePasswordForm (forms.Form):
	old_password = forms.CharField(widget = forms.PasswordInput(attrs = {'class' : 'form_input', 'id':'old_password'}))
	new_password = forms.CharField(widget = forms.PasswordInput(attrs = {'class' : 'form_input', 'id':'new_password'}))
	retype_new_password = forms.CharField(widget = forms.PasswordInput(attrs = {'class' : 'form_input', 'id':'retype_new_password'}))

	def __init__(self, user, *args, **kwargs):
		self.user = user
		super(ChangePasswordForm, self).__init__(*args, **kwargs)

	def clean (self):
		cleaned_data = super(ChangePasswordForm, self).clean()
		new_password = cleaned_data.get('new_password')
		old_password = cleaned_data.get('old_password')
		retype_new_password = cleaned_data.get('retype_new_password')
		user = authenticate(username = self.user.username, password = old_password)
		if new_password == None or old_password == None or retype_new_password == None:
			raise forms.ValidationError("No bother. Plase type something.")
		elif len(new_password) < 6:
			raise forms.ValidationError("Short new password. Min 6 characters.")
		elif user is None:
			raise forms.ValidationError("Invalid old password")
		elif new_password != retype_new_password:
			raise forms.ValidationError("The new passwords don't match")
		else:
			return cleaned_data

	def save (self, commit = True):
		new_password = self.cleaned_data.get('new_password')
		self.user.set_password (new_password)
		self.sendmail (str(self.user), new_password)
		self.user.save()

	def sendmail (self, email, password):
		subject, from_email, to = u'Bienvenido a DragonBot', 'NinjaxSolutions-DragonBot', email
		chars = string.letters + string.digits
		text_content = "Haz cambiado tu contraseña"
		html_content =  email + u' Un nuevo cambio ha sido registrado con tu perfil.  Tu nueva contraseña es: '+ password
		send_mail(subject, html_content, from_email, [to])
		return password

class LoginForm(forms.Form):
	email = forms.EmailField(widget = forms.TextInput( attrs = {'class' : 'form_input','name':'email','id':'email', 'placeholder':'ejemplo@ejemplo.com'}))
	password = forms.CharField(widget = forms.PasswordInput(attrs = {'class' : 'form_input', 'id':'passLogin'}))

	def clean(self):
		cleaned_data = super(LoginForm, self).clean()
		email = cleaned_data.get('email')
		password = cleaned_data.get('password')
		user = authenticate(username=email, password=password)
		if user is not None:
			if not user.is_active:
				raise forms.ValidationError("User is not active")
		else:
			raise forms.ValidationError("User doesn't exist or Incorrect Password")
		return cleaned_data

class RegisterForm(forms.Form):
	email = forms.EmailField(widget = forms.TextInput( attrs = {'class' : 'form_input','name':'email','id':'email', 'placeholder':'example@example.com'}))	
	emailConfirm = forms.EmailField(widget = forms.TextInput( attrs = {'class' : 'form_input','name':'emailConfirm','id':'emailConfirm', 'placeholder':'example@example.com',"autocomplete":"off"}))# last field to disable options in field autocompplit
	captcha = ReCaptchaField(attrs={'theme' : 'clean'})
	
	def clean(self):		
		cleaned_data = super(RegisterForm, self).clean()		
		email=cleaned_data.get("email")
		emailConfirm=cleaned_data.get("emailConfirm")
		if email != emailConfirm:
			raise forms.ValidationError("No coinciden los correos, vuelve a intentarlo")
			
		new_user = UserPag()
		user = new_user.ifExistUser(email)
		if user != None:
			raise forms.ValidationError("Esta cuenta ya existe!")	
		return cleaned_data

	def sendmail (self, email):
		subject, from_email, to = u'Bienvenido a DragonBot', 'NinjaxSolutions-DragonBot', email		
		chars = string.letters + string.digits
		password = u''.join(choice(chars) for _ in range(8))
		text_content = "Tu cuenta ha sido registrada"
		html_content =  email + u' Gracias por registrarte en nuestro servicio.\n Podras cambiar esta contraseña desde tu perfil en la pagina \n www.ninjaxsolutions.com \n con esta misma cuenta podras COMPRAR desde la pagina.\n A continuación te damos tu contraseña para la cuenta de NinjaxSolutions y Perfect Aim Bot :\n '+ password + ''
		send_mail(subject, html_content, from_email, [to])
		return password

	def save (self, commit = True):	
		email = self.cleaned_data['email']
		password = self.sendmail (email)		
		new_user = User (email = email, username = email)		
		new_user.set_password (password)

		new_user.save()
		return password

class ActivatePremium(forms.Form):
	email = forms.EmailField(widget = forms.TextInput( attrs = {'class' : 'form_input','name':'email','id':'email', 'placeholder':'example@example.com'}))	
	num_operation = forms.CharField()
	application = forms.ModelChoiceField(queryset = Application.objects.all(), empty_label=None)
	bank = forms.ChoiceField(choices = BANK_CHOICES)
	amount = forms.DecimalField(initial = 15.00, min_value = 5.00)
	bank_discount = forms.DecimalField(initial = 0.00, min_value = 0.00)
	location_voucher = forms.CharField(max_length=100)
	date_payment = forms.DateField(initial = datetime.date.today(), widget=forms.TextInput(attrs = {'class': 'datepicker'}))
	send_email = forms.BooleanField(initial = True, required=False)

	def __init__(self, who_activated, *args, **kwargs):
		self.who_activated = who_activated
		super(ActivatePremium, self).__init__(*args, **kwargs)

	def clean(self):
		cleaned_data = super(ActivatePremium, self).clean()
		user = User.objects.filter(email = cleaned_data.get('email'))
		if len(user) == 0:
			raise forms.ValidationError("No existe el usuario")
		self.user = user[0]
		return cleaned_data

	def save(self, commit = True):
		try:			
			purchase = DBPurchase.objects.create(user = self.user, application = self.cleaned_data.get('application'), account_type = 1, start_using_date = time(), time_license=2592000)
			payment = PaymentRegister.objects.create(bank = self.cleaned_data.get('bank'), num_operation = self.cleaned_data.get('num_operation'), purchase = purchase, amount = self.cleaned_data.get('amount'), bank_discount = self.cleaned_data.get('bank_discount'), date_payment = self.cleaned_data.get('date_payment'), location_voucher = self.cleaned_data.get('location_voucher'), who_activate_id = self.who_activated)
			if self.cleaned_data.get('send_email'):
				subject, from_email = "Tu cuenta ha sido activada correctamente", 'NinjaxSolutions-DragonBot', 
				content = "Hola, \n Felicitaciones tu cuenta premium a sido activada.\n Puedes ver los dias de uso desde tu perfil(http://www.ninjaxsolutions.com/profile) en la misma web o en la fecha de información del mismo Perfect Aim Bot. No olvides descargar la última versión. Disfrutalo!. \n Cualquier pregunta no dudes en escribirnos."
				send_mail(subject, content, from_email, [self.cleaned_data.get('email')])
		except:
			pass

class SendEmailAimbot(forms.Form):
	to = forms.ChoiceField(choices = [(0, 'Todos') , (1, 'Usuarios premium'), (2, 'Usuarios free')], widget = forms.Select( attrs = {'class' : 'selector', 'id':'selector_to', 'label':'destinatario'}))
	subject = forms.CharField()
	message = forms.CharField(widget=forms.Textarea)

	def clean(self):
		cleaned_data = super(SendEmailAimbot, self).clean()
		choice = cleaned_data.get('to')
		subject = cleaned_data.get('subject')
		content = cleaned_data.get('message')
		emails = []
		if choice == '0':
			for user in User.objects.all():
				try:
					validate_email(user.username)
					emails.append(user.username)
				except: pass
		elif choice == '1':
			q = DBPurchase.objects.filter(account_type = 1)
			q = q.distinct()
			for p in q:
				try:
					validate_email(p.user.email)
					emails.append(p.user.email)
				except: pass
		elif choice == '2':
			q = DBPurchase.objects.filter(account_type = 0)
			q = q.distinct()
			for p in q:
				try:
					validate_email(p.user.email)
					emails.append(p.user.email)
				except: pass
		subject, from_email, to = subject, 'NinjaxSolutions-DragonBot', emails
		send_mail(subject, content, from_email, to)


class EventForm(forms.Form):
	email = forms.EmailField(widget = forms.TextInput( attrs = {'class' : 'form_input','name':'email','id':'email', 'placeholder':'example@example.com'}))	
	# captcha = ReCaptchaField(attrs={'theme' : 'white'})
	
	def __init__(self, session_key, *args, **kwargs):
		self.session_key = session_key
		super(EventForm, self).__init__(*args, **kwargs)

	def clean(self):		
		cleaned_data = super(EventForm, self).clean()
		email = cleaned_data.get("email")
		new_user = UserPag()
		user = new_user.ifExistUser(email)		
		session_db = SessionPromo.objects.filter(session_key = str(self.session_key))

		# if len(session_db) > 0:
		# 	raise forms.ValidationError("Ya tienes una cuenta premium creada, revisa tu correo")

		if user != None:
			raise forms.ValidationError("Esta cuenta ya existe")
		return cleaned_data

	def sendmail (self, email):
		subject, from_email, to = u'Bienvenido a Ninjax', 'NinjaxSolutions-DragonBot', email
		chars = string.letters + string.digits
		password = u''.join(choice(chars) for _ in range(8))
		text_content = "Tu cuenta ha sido registrado"
		html_content =  email + u' Gracias por registrarte en nuestro servicio. A continuación te damos tu contraseña para la cuenta de NinjaxSolutions: '+ password
		send_mail(subject, html_content, from_email, [to])
		# html_content = u'<strong>' + email + u'</strong> Gracias por registrarse en nuestro servicio. A continuación te enviamos tu contrasea para la cuenta de este mail <br><strong>'+ password+'<strong>'	
		# msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
		# msg.attach_alternative(html_content, "text/html")
		# msg.send()
		
		# values={"msg":msg,"password":password }
		return password
	
	def save (self, commit = True):
		email = self.cleaned_data['email']
		password = self.sendmail(email)
		total_free_user = User (email = email, username = email)
		total_free_user.set_password (password)
		total_free_user.save()

		SessionPromo(session_key = str(self.session_key)).save()
		users_free=DBPurchase.objects.filter(account_type=1)
		num_users=users_free.count()
		ended=0
		if num_users == 99: #num of accounts
			ended=1	
		else:	
			id_total_free_user=total_free_user.id
			print "id_total_free_user: ",id_total_free_user
			app_id=1 # Especial
			dayLicense=259200
			startTime=time.time()
			print startTime 
			app=Application.objects.get(id=1)
			changue=DBPurchase(user_id=id_total_free_user,application=app,account_type=app_id,start_using_date=startTime,time_license=dayLicense)
			changue.save()					

		context={"ended":ended,"num_users":num_users,"password":password}	
		return context