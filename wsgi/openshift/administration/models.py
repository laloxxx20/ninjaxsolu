import datetime
from django.utils import timezone
from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from time import time
import datetime

ACCOUNTSTYPECHOICE = (
 (0, 'Free'),
 (1, 'Premium'),
)
# DB, db = DragonBound

class AppCategory (models.Model):
	name = models.CharField(max_length=50)
	description = models.CharField(max_length=100)

	def __unicode__ (self):
		return self.name

class Application (models.Model):
	name = models.CharField(max_length=50)
	description = models.TextField()
	category = models.ForeignKey(AppCategory)

	def __unicode__ (self):
		return self.name

class DBPurchase(models.Model):
	"""
		All times are given in epoch format
		start_using_date = The time that the user uses for first time the bot
		time_license = Time of buying. Free = 1 day, Premium = 1 Month
		is_expired = A value that determine if the buy has expired
	"""
	user = models.ForeignKey(User)
	application = models.ForeignKey(Application)
	account_type = models.IntegerField(choices=ACCOUNTSTYPECHOICE, default=0)
	start_using_date = models.IntegerField(default=0, blank=True, null=True)
	time_license = models.IntegerField(default=86400)
	is_expired = models.BooleanField(default = False)

	def __unicode__ (self):
		string = "PK: %i, User: %s, account_type: %i " % (self.pk, self.user.email, self.account_type)		
		return string

class DBAccountPurchase (models.Model):
	"""
		This table stores the purchases with Dragonbound Account. For Premium is allow 5. For Frees is 1.
	"""
	db_purchase = models.ForeignKey(DBPurchase)
	db_account_id = models.CharField(max_length=100)
	def __unicode__ (self):
		return "Purchase %i, DB_Account %i" % (self.db_purchase.pk, self.db_account_id)

class SessionPromo (models.Model):
	"""
		Stores all the id's session in order to not repeated.
	"""
	session_key = models.CharField(max_length=100)

	def __unicode__(self):
		return self.session_key

class FailureTime (models.Model):
	"""
		This table stores all the times where the bot doesn't work.
	"""
	application = models.ForeignKey(Application)
	date = models.IntegerField()
	time = models.IntegerField()

BANK_CHOICES = (
 (0, 'Interbank'),
 (1, 'BCP'),
 (2, 'BBVA'),
)

class PaymentRegister (models.Model):	
	who_activate = models.ForeignKey(User, related_name='who_activate', blank=True, null = True)
	bank = models.IntegerField(choices=BANK_CHOICES, default=0)
	num_operation = models.CharField(max_length=100)
	purchase = models.ForeignKey(DBPurchase)
	amount = models.DecimalField(max_digits=8, decimal_places=2)
	bank_discount = models.DecimalField(max_digits=8, decimal_places=2)
	location_voucher = models.CharField(max_length=100)
	date_payment = models.DateField(default = datetime.date.today())

class Paypal(models.Model):
	user = models.ForeignKey(User)
	payment_status=models.CharField(max_length=100)
	receiver_email=models.CharField(max_length=100)
	payment_sail=models.DecimalField(max_digits=8, decimal_places=2)
	txn_id=models.CharField(max_length=100)
	residence_country=models.CharField(max_length=100)
	# payment_day=models.DateTimeField(input_formats='%Y-%m-%d %H:%M')#input_formats=['%H:%M:%S %d %b, %Y'])
	item_name=models.CharField(max_length=100)
	payer_id=models.CharField(max_length=100)
	ipn_track_id=models.CharField(max_length=100)
	active_confirm_purcha=models.BooleanField(default = False) #field to now if is the firstTime to pag. confirmPurchase.html

class Post(models.Model):
	user = models.ForeignKey(User)
	title= models.CharField(max_length=200)
	content = models.TextField()
	tags = models.CharField(max_length=100)
	urlImage = models.TextField()
	pub_date = models.DateTimeField('date published')	
	def __unicode__(self):
		 return self.title