# -*- coding: utf-8 -*-
import datetime

from django import template

register = template.Library()

@register.filter(name='fromunix')
def fromunix(value):
    return datetime.datetime.fromtimestamp(int(value))

@register.filter(name='time_in_days')
def time_in_days(value):
	real_time = int(value)/86400
	if real_time >= 360:
		real_time = real_time/360
		if real_time == 1: return "%i año" % real_time
		else: return "%i años" % real_time
	elif real_time >= 30:
		real_time = real_time/30
		if real_time == 1: return "%i mes" % real_time
		else: return "%i meses" % real_time
	else:
		if real_time == 1: return "%i día" % real_time
		else: return "%i días" % real_time