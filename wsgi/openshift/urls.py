from django.conf.urls.defaults import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

handler404 = 'mysite.views.handler404'


urlpatterns = patterns('',
    # Examples:
    #url(r'^$', 'openshift.views.home', name='home'),
    # url(r'^openshift/', include('openshift.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^adminzone/', include(admin.site.urls)),
    url(r'^', include('administration.urls', namespace="administration")), #here go /administration beside r'^'
    # url(r'^payments/', include('payment.standard.ipn.urls')),
    # url(r'^captcha/', include('captcha.urls')),
    
    #url(r'^accounts/', include('userena.urls')),  

    #url(r'^accounts/', include('registration.backends.default.urls')),
)
