# -*- coding: utf-8 -*-
# Django settings for openshift project.
import imp,os

# a setting to determine the username
os_username = os.getlogin()
if os_username == 'josue':
	password_db = '1234'
else:	password_db = ''
# a setting to determine whether we are running on OpenShift
ON_OPENSHIFT = False
if os.environ.has_key('OPENSHIFT_REPO_DIR'):
	ON_OPENSHIFT = True

PROJECT_DIR = os.path.dirname(os.path.realpath(__file__))

if ON_OPENSHIFT:
	DEBUG = False
else:
	DEBUG = True

TEMPLATE_DEBUG = DEBUG

if not DEBUG:
	if ON_OPENSHIFT:
		ALLOWED_HOSTS = ['.ninjaxsolutions.com','.paypal.com/cgi-bin/webscr','.sandbox.paypal.com/cgi-bin/webscr']
		#ALLOWED_HOSTS = ['*']
	else:
		ALLOWED_HOSTS = ['*']

ADMINS = (
	# ('Your Name', 'your_email@example.com'),
)
MANAGERS = ADMINS

if ON_OPENSHIFT:
	# os.environ['OPENSHIFT_MYSQL_DB_*'] variables can be used with databases created
	# with rhc cartridge add (see /README in this git repo)
	DATABASES = {
		'default': {
			'ENGINE': 'django.db.backends.mysql',  # Add 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
			#'NAME': os.path.join(os.environ['OPENSHIFT_DATA_DIR'], 'sqlite3.db'),  # Or path to database file if using sqlite3.
			# 'NAME': 'ninjax',    
			# 'USER': 'root',                      # Not used with sqlite3.
			# 'PASSWORD': '',                  # Not used with sqlite3.
			'NAME': 'ninjax',    
			'USER': os.environ['OPENSHIFT_MYSQL_DB_USERNAME'],
			# 'PASSWORD': os.environ['OPENSHIFT_MYSQL_DB_PASSWORD'],    
			'PASSWORD':'AsRly5ENZPI7',
			'HOST': os.environ['OPENSHIFT_MYSQL_DB_HOST'],                      # Set to empty string for localhost. Not used with sqlite3.
			'PORT': os.environ['OPENSHIFT_MYSQL_DB_PORT'],                      # Set to empty string for default. Not used with sqlite3.			
		}
	}
else:
	DATABASES = {
		'default': {
			'ENGINE': 'django.db.backends.mysql',  # Add 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
			#'NAME': os.path.join(PROJECT_DIR, 'sqlite3.db'),  # Or path to database file if using sqlite3.
			'NAME': 'ninjax',    
			'USER': 'root',
			'PASSWORD': password_db,                  # Not used with sqlite3.
			'HOST': '',                      # Set to empty string for localhost. Not used with sqlite3.
			'PORT': '',                      # Set to empty string for default. Not used with sqlite3.
		}
	}

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# On Unix systems, a value of None will cause Django to use the same
# timezone as the operating system.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = 'America/Chicago'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-us'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale
USE_L10N = True

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/media/"
MEDIA_ROOT = os.environ.get('OPENSHIFT_DATA_DIR', '')

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://media.lawrence.com/media/", "http://example.com/media/"
MEDIA_URL = ''

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/home/media/media.lawrence.com/static/"
STATIC_ROOT = os.path.join(PROJECT_DIR, '..', 'static')

# URL prefix for static files.
# Example: "http://media.lawrence.com/static/"
STATIC_URL = '/static/'

# URL prefix for admin static files -- CSS, JavaScript and images.
# Make sure to use a trailing slash.
# Examples: "http://foo.com/static/admin/", "/static/admin/".
ADMIN_MEDIA_PREFIX = '/static/admin/'

# Additional locations of static files
STATICFILES_DIRS = (
	os.path.join(PROJECT_DIR, 'templates/templateNinjax'),
	# Put strings here, like "/home/html/static" or "C:/www/django/static".
	# Always use forward slashes, even on Windows.
	# Don't forget to use absolute paths, not relative paths.
)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
	'django.contrib.staticfiles.finders.FileSystemFinder',
	'django.contrib.staticfiles.finders.AppDirectoriesFinder',
	#'django.contrib.staticfiles.finders.DefaultStorageFinder',
)
# Make a dictionary of default keys
default_keys = { 'SECRET_KEY': 'vm4rl5*ymb@2&d_(gc$gb-^twq9w(u69hi--%$5xrh!xk(t%hw' }
# Replace default keys with dynamic values if we are in OpenShift
use_keys = default_keys
if ON_OPENSHIFT:
	imp.find_module('openshiftlibs')
	import openshiftlibs
	use_keys = openshiftlibs.openshift_secure(default_keys)

# Make this unique, and don't share it with anybody.
SECRET_KEY = use_keys['SECRET_KEY']

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
	'django.template.loaders.filesystem.Loader',
	'django.template.loaders.app_directories.Loader',
	#'django.template.loaders.eggs.Loader',
)

MIDDLEWARE_CLASSES = (
	'django.middleware.cache.UpdateCacheMiddleware',
	'django.contrib.sessions.middleware.SessionMiddleware',
	'django.middleware.common.CommonMiddleware',
	'django.middleware.transaction.TransactionMiddleware',
	'django.middleware.cache.FetchFromCacheMiddleware',	
	'django.middleware.csrf.CsrfViewMiddleware',
	'django.contrib.auth.middleware.AuthenticationMiddleware',
	'django.contrib.messages.middleware.MessageMiddleware',
)

ROOT_URLCONF = 'openshift.urls'

TEMPLATE_DIRS = (
	# Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
	# Always use forward slashes, even on Windows.
	# Don't forget to use absolute paths, not relative paths.
	os.path.join(PROJECT_DIR, 'templates'),
	os.path.join(PROJECT_DIR, 'templates/templateNinjax'),
)

#Paypal Lib Variables 
PAYPAL_RECEIVER_EMAIL = 'ninjaxxx@yopmail.com'
PAYPAL_IDENTITY_TOKEN = "HlGubFu8oI2JUWCpGZORB5y39GNmOE44NiSyDiLFqUFkkABQu7yvgU60OkO"

INSTALLED_APPS = (
	'captcha',
	'django.contrib.auth',
	'django.contrib.contenttypes',
	'django.contrib.sessions',
	'django.contrib.sites',
	'django.contrib.messages',
	'django.contrib.staticfiles',
	'django.contrib.admin',
	# 'payment.standard.ipn',
	'administration',
	'django_extensions',
	# 'accounts',
	# 'django.contrib.admindocs',
)

LOGIN_URL = 'administration:login'

EMAIL_USE_TLS = True  
EMAIL_HOST = 'smtp.gmail.com'  
EMAIL_PORT = 25
EMAIL_HOST_USER = 'ninjaxcompany@gmail.com'  
EMAIL_HOST_PASSWORD = 'ktsyazlttguxhisw'   #code  genereted by gmail just ot applications

#since here django registration
AUTH_PROFILE_MODULE = 'administration.UserProfile' # is for new data for User object

#server
RECAPTCHA_PUBLIC_KEY = '6Lc0COcSAAAAAFWgHQjfhC6ffEWiOHGA-wixFG-b'
RECAPTCHA_PRIVATE_KEY = '6Lc0COcSAAAAAAOslTugXDSwm5dV39d3F2XPlJQY'
RECAPTCHA_USE_SSL = True

# USE_L10N=True


LOGGING = {
	'version': 1,
	'disable_existing_loggers': False,
	'handlers': {
		'mail_admins': {
			'level': 'ERROR',
			'class': 'django.utils.log.AdminEmailHandler'
		}
	},
	'loggers': {
		'django.request': {
			'handlers': ['mail_admins'],
			'level': 'ERROR',
			'propagate': True,
		},
	}
}
